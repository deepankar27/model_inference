import uvicorn
import os, sys
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.responses import JSONResponse
from datetime import datetime, date
from typing import List, Optional, Union
from starlette.middleware.cors import CORSMiddleware
from typing import List, Optional, Union

#from Data_Layer import data_model
#from Logic_layer import main
import time
import pika

tags_metadata = [
    
     {"name": "Ping",
     "description": "This API is to ping and check available or not",
     }
]

app = FastAPI(title='Model module', description='This API is for inferencing embeddings', version='1.0',
              openapi_tags=tags_metadata)

app.add_middleware(CORSMiddleware,
                   allow_origins=["*"],  # Allows all origins
                   allow_credentials=True,
                   allow_methods=["*"],  # Allows all methods
                   allow_headers=["*"])  # Allows all headers



@app.post("/ping", tags=["Ping"])
def Ping(sentences: str):
    t0= time.time()
    
    t1 = time.time() - t0
    print("Time elapsed: ", t1)
    return {'Result':'Hello I am alive'}

# if __name__ == '__main__':
#     uvicorn.run(app, host="0.0.0.0", port=8027)